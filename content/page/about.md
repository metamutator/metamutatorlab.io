---
title: About me
subtitle: నా గురించి
comments: false
---

Hi, I'm Akshay. Born in India, live in Singapore, travel the world. I speak Telugu, English, Hindi/ Urdu, French and Spanish, in a rough order of fluency. On some days, my Spanish is better, on others French. _C'est la vie_.  También depende de la multitud.

I have a first degree in Computing and Mathematics from National University of Singapore and a later MBA degree from Nanyang Business School in Singapore and IESE Business School in Barcelona. I use this site mostly to practise the art of _data storytelling_: explore common hypotheses with datasets, nice visualizations and hopefully, a good narrative. There will be _some_ Asian focus, but not exclusively so!

# my history

## Professional

My [LinkedIn profile](www.linkedin.com/in/akshr) has a good summary of my work-experience.

## Personal

I have interests in math, languages and photography.